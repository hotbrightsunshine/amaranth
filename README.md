# 🪴🐻 Amaranth
Amaranth is a small drawing app written in Haskell. It was
created as a small learning project, and it probably won't
be continued. 

This code is licensed with The Unlicense. Learn more at 
https://unlicense.org/.

# ⚙️ How to use 
Amaranth comes with two modes with which you can use to draw on 
the screen:
- **brush mode**, which you can use by hitting B on the keyboard, and
- **eraser mode**, which you can use by pressing E. 

# 📸 Screenshots
None by now. 😥