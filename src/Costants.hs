module Costants where

import Canvas
import Graphics.Gloss


display :: Display
display = InWindow "Amaranth" windowSize (0,0)

background :: Color 
background = white

initial :: Canvas
initial = Canvas {
    items   =   [] 
,   mode    =   Brush
,   drawing =   False
,   pos     =   (0, 0)
,   tip     =   Tip Rectangular 3
}