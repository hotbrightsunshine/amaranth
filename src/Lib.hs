module Lib where

import Graphics.Gloss
import Canvas
import Costants 

app :: IO ()
app = play 
      Costants.display      -- Display type
      Costants.background   -- Background
      60                    -- FPS
      Costants.initial      -- Initial world
      Canvas.render         -- Render function
      Canvas.handle         -- Input hadler
      Canvas.update         -- Update function (unused, since we don't have to make animations)
